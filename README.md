# Deploy to S3 Bucket

An [example script](https://bitbucket.org/ian_buchanan/pipeline-example-aws-s3/src/master/deploy-to-s3.bash)
for deploying from Bitbucket Pipelines to an AWS S3 Bucket.

## How to use it

* Add required environment variables to your Bitbucket enviroment variables.
* Make sure your build script already zips (or similar) your code.
* Copy `deploy-to-s3.bash` to your project.
* Add `deploy-to-s3.bash` to your build configuration.


## Required environment variables

* **`S3_SOURCE_FILE`**: (Required) Source file to upload to S3. Example: `target/writelogevent.zip`
* **`S3_SOURCE_CONTENT_TYPE`**: (Required) MIME content-type of the source file. Example: `application/zip`
* **`AWS_S3_TARGET_FILE`**: (Required) Target file in S3. Example: `writelogevent.zip`
* **`AWS_S3_BUCKET`**: (Required) Target AWS S3 bucket for upload.
* **`AWS_ACCESS_KEY_ID`**: (Required) Access Key ID for the target AWS account.
* **`AWS_SECRET_ACCESS_KEY`**: (Required, Secret) Secret Access Key for the target AWS account.

For more information about AWS access tokens see: http://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSGettingStartedGuide/AWSCredentials.html

## Deploy to S3

```
RESOURCE="/${AWS_S3_BUCKET}/${AWS_S3_BUCKET}/${AWS_S3_TARGET_FILE}"
DATE_VALUE=`date -R`
STRING_TO_SIGN="PUT\n\n${S3_SOURCE_CONTENT_TYPE}\n${DATE_VALUE}\n${RESOURCE}"
SIGNATURE=`echo -en ${STRING_TO_SIGN} | openssl sha1 -hmac ${AWS_SECRET_ACCESS_KEY} -binary | base64`
curl -X PUT -T "${S3_SOURCE_FILE}" \
  -H "Host: ${AWS_S3_BUCKET}.s3.amazonaws.com" \
  -H "Date: ${DATE_VALUE}" \
  -H "Content-Type: ${S3_SOURCE_CONTENT_TYPE}" \
  -H "Authorization: AWS ${AWS_ACCESS_KEY_ID}:${SIGNATURE}" \
  https://s3.amazonaws.com/${AWS_S3_BUCKET}/${AWS_S3_TARGET_FILE}
```

* Uses `-ex` switches (not shown in snippet above) to exit on first error and to echo command before execution. These help with debugging.
* Uses Open SSL and Base64 to create an HMAC signature from HTTP verb (PUT), content type, date, and resource path.
* Uses Curl to make an HTTP PUT request with the HMAC signature, content type, date, and the S3 bucket, sending the source file as the payload.

## Build configuration

```
pipelines:
    default:
        - step:
            script:
                - apt-get install zip
                - ./zip.bash
                - ./deploy-to-s3.bash
```

* Uses the default Atlassian image, which already contains Apt-get, Bash, Open SSL, Base64, and Curl.
* Installs Zip, since it is not part of the default image.
* Zips the source code (in a way suitable for deployment as an AWS Lambda).
* Deploys the zip file to the S3 bucket.
