#!/bin/bash -ex

# S3_SOURCE_FILE            (Required) Source file to upload to S3
#                               Example: "target/writelogevent.zip"
# S3_SOURCE_CONTENT_TYPE    (Required) MIME content-type of the source file
#                               Example: "application/zip"
# AWS_S3_TARGET_FILE        (Required) Target file in S3
#                               Example: "writelogevent.zip"
# AWS_S3_BUCKET             (Required) Target AWS S3 bucket for upload
# AWS_ACCESS_KEY_ID         (Required) Access Key ID for the target AWS account
# AWS_SECRET_ACCESS_KEY     (Required, Secret) Secret Access Key for the target AWS account
# See: http://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSGettingStartedGuide/AWSCredentials.html


RESOURCE="/${AWS_S3_BUCKET}/${AWS_S3_BUCKET}/${AWS_S3_TARGET_FILE}"
DATE_VALUE=`date -R`
STRING_TO_SIGN="PUT\n\n${S3_SOURCE_CONTENT_TYPE}\n${DATE_VALUE}\n${RESOURCE}"
SIGNATURE=`echo -en ${STRING_TO_SIGN} | openssl sha1 -hmac ${AWS_SECRET_ACCESS_KEY} -binary | base64`
curl -X PUT -T "${S3_SOURCE_FILE}" \
  -H "Host: ${AWS_S3_BUCKET}.s3.amazonaws.com" \
  -H "Date: ${DATE_VALUE}" \
  -H "Content-Type: ${S3_SOURCE_CONTENT_TYPE}" \
  -H "Authorization: AWS ${AWS_ACCESS_KEY_ID}:${SIGNATURE}" \
  https://s3.amazonaws.com/${AWS_S3_BUCKET}/${AWS_S3_TARGET_FILE}
